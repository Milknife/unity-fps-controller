﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().AddTorque(Vector3.up, ForceMode.VelocityChange);
    }
}
