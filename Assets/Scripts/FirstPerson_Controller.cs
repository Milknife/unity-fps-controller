﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class works through a managed polling of input, unity character controller,
/// and a mass for calculating force applied upon collision.
/// </summary>
[RequireComponent(typeof(CharacterController))]
public class FirstPerson_Controller : MonoBehaviour
{

    #region InputBinds
    /// <summary>
    /// Axis to look left and right
    /// </summary>
    public string m_AxisLookX = "Mouse X";

    /// <summary>
    /// Axis to look up and down
    /// </summary>
    public string m_AxisLookY = "Mouse Y";

    /// <summary>
    /// Axis to move forwards and backwards - relative space
    /// </summary>
    public string m_AxisMoveZ = "Vertical";

    /// <summary>
    /// Axis to move left and right - relative space
    /// </summary>
    public string m_AxisMoveX = "Horizontal";

    /// <summary>
    /// Primary fire button
    /// </summary>
    public string m_BtnFire = "Fire";

    /// <summary>
    /// Alternate fire button
    /// </summary>
    public string m_BtnAltFire = "Alt Fire";

    /// <summary>
    /// Melee fire button
    /// </summary>
    public string m_BtnMelee = "Melee";

    /// <summary>
    /// Reload button
    /// </summary>
    public string m_BtnReload = "Reload";

    /// <summary>
    /// Use object
    /// </summary>
    public string m_BtnUse = "Use";

    /// <summary>
    /// Use object
    /// </summary>
    public string m_BtnManipulate = "Manipulate";

    /// <summary>
    /// Interface/Menu button
    /// </summary>
    public string m_BtnInterface = "Interface";

    /// <summary>
    /// Crouch button
    /// </summary>
    public string m_BtnCrouch = "Crouch";

    /// <summary>
    /// Prone button
    /// </summary>
    public string m_BtnProne = "Prone";

    /// <summary>
    /// Walk button
    /// </summary>
    public string m_BtnWalk = "Walk";

    /// <summary>
    /// Sprint button
    /// </summary>
    public string m_BtnSprint = "Sprint";

    /// <summary>
    /// Jump button
    /// </summary>
    public string m_BtnJump = "Jump";
    #endregion


    /// <summary>
    /// Referent to the POV Camera for looking up and down
    /// </summary>
    public float m_fLookSensitivity;


    #region Player's Physicals Properties
    /// <summary>
    /// Character mass in kgs
    /// </summary>
    public float m_fMass = 90.0f;

 

    /// <summary>
    /// Strength accessor for maximum physics interaction weights
    /// </summary>
    #endregion

    #region Camera
    /// <summary>
    /// Referent to the POV Camera for looking up and down
    /// </summary>
    public GameObject m_POVCamera;
    #endregion
    #region Colliders and Collision Registration
    /// <summary>
    /// The character controller that this script is attached to
    /// </summary>
    public CharacterController m_CharacterController;
    /// <summary>
    /// Script attached to child object with collider that occupies the standing volume.
    /// </summary>
    public FirstPerson_CollisionRegister m_HeightStand;
    /// <summary>
    /// Script attached to child object with collider that occupies the crouch volume.
    /// </summary>
    public FirstPerson_CollisionRegister m_HeightCrouch;
    /// <summary>
    /// Script attached to child object with collider that occupies the crouch volume.
    /// </summary>
    public FirstPerson_CollisionRegister m_HeightProne;

    /// <summary>
    /// Script attached to child object with collider that occupies the head volume.
    /// </summary>
    public FirstPerson_CollisionRegister m_HeightJump;
    /// <summary>
    /// Script attached to child object with collider that occupies the feet volume.
    /// </summary>
    public FirstPerson_CollisionRegister m_HeightGrounded;

    #endregion

   

    


    #region Pose States and Values
    /// <summary>
    /// State indicating height of the controller and view
    /// </summary>
    private enum Pose_State
    {
        Stand,
        Crouch,
        Prone
    }

    /// <summary>
    /// Current character volume
    /// </summary>
    private Pose_State m_ePose = Pose_State.Stand;

    /// <summary>
    /// Previous character volume 
    /// - Used to ensure if Crouch to prone then releasing crouch does not go to stand. 
    /// </summary>
    private Pose_State m_ePreviousPose = Pose_State.Stand;

    /// <summary>
    /// Flag for whether lerping between poses
    /// </summary>
    private bool m_bPoseLerping = false;

    /// <summary>
    /// How long the player has been lerping between heights
    /// </summary>
    private float m_fPoseTimer = 0.0f;

    /// <summary>
    /// Time taken to lerp between heights
    /// </summary>
    private float m_fPoseTime = 0.25f;

    #endregion

    /// <summary>
    /// State indicating whether character is jumping
    /// </summary>
    private enum Jump_State
    {
        Grounded,
        Jumping,
        InAir // Can be up or down but no longer activley.
    }


    private enum Grounded_State
    {
        AirBorne,
        Flat,
        Slope,
        Wall,
        Step
    }




    #region Speeds

    /// <summary>
    /// Player Crawl Speed - m/s
    /// </summary>
    public float m_fProneSpeed = 1.0f;
    /// <summary>
    /// Player Crouch Speed - m/s
    /// </summary>
    public float m_fCrouchSpeed = 2.0f;


    /// <summary>
    /// Player Walk Speed - m/s
    /// </summary>
    public float m_fWalkSpeed = 2.5f;
    /// <summary>
    /// Player Run Speed - m/s
    /// </summary>
    public float m_fRunSpeed = 6.0f;
    /// <summary>
    /// Player Sprint Speed - m/s
    /// </summary>
    public float m_fSprintSpeed = 10.0f;

    /* Speed ratios for motion along local X and -Z axis */
    /// <summary>
    /// Local X Axis speed is this ratio of current state speed
    /// </summary>
    public float m_fStrafeRatio = 0.8f;
    /// <summary>
    /// Local Negative Z Axis speed is this ratio of current state speed
    /// </summary>
    public float m_fReverseRatio = 0.65f;
    /// <summary>
    /// In air speed limit on X/Z as a ratio of current state speeds
    /// </summary>
    public float m_fAirborneRatio = 0.75f;// This should be a clamp, not a ratio

    /* Jump values */
    /// <summary>
    /// Y speed impulse on jump - m/s
    /// </summary>
    public float m_fJumpSpeed = 4.25f;
    /// <summary>
    /// Ratio of jump impulse if crouching
    /// </summary>
    public float m_fCrouchJumpRatio = 0.75f;
    /// <summary>
    /// Ratio of jump impulse if prone
    /// </summary>
    public float m_fProneJumpRatio = 0.65f;


    public float m_fStepSpeed = 0.25f;
    public float m_fStepHeight = 0.50f;

 
    #endregion

    #region Motion
    /* Player motion co-efficients */
    /// <summary>
    /// Player gravity taken from world setup
    /// </summary>
    private float m_fGravity = Physics.gravity.y;

    /// <summary>
    /// Public read-only player gravity
    /// </summary>
    public float Gravity
    {
        get { return m_fGravity; }
    }

    /// <summary>
    /// Current Y velocity of the player
    /// </summary>
    private float m_fYVelocity = 0.0f;
    
    /// <summary>
    /// Current input velocity of the player
    /// </summary>
    private Vector3 m_v3Motion = Vector3.zero;

    /// <summary>
    /// Launch velocity for minimising air steering
    /// </summary>
    private Vector3 m_v3LaunchVelocity;

    /// <summary>
    /// Local temporary vector 3 for calculation
    /// </summary>
    private Vector3 t_Vector3;

    /// <summary>
    /// Temporary rigid body for current collidable
    /// </summary>
    private Rigidbody t_RigidBody;

    /// <summary>
    /// Temporary float storage of the dot product
    /// </summary>
    private float t_fDot;

    /// <summary>
    /// Const radians in a degree
    /// </summary>
    private const float RADIANS_TO_DEGREES = 0.0174533f;

    /// <summary>
    /// Const radians in a degree
    /// </summary>
    private const float ROTATION_PADDING = 1.0f;

    #endregion

    bool stepping = false;
    private float m_fSkinThickness = 0.01f;
    /* Speeds for all mobility states in m/s */
    private float m_fCurrentSpeed = 0.0f;

    /// <summary>
    /// How deep into the controller, the camera sits.
    /// </summary>
    private float m_fEyeDepthRatio = 0.15f;
    /// <summary>
    /// Maximumm weight a player can push, pull in any direction
    /// </summary>
    private float m_fPushWeight = 250.0f; // Shouldnt this be speed * weight?

    /// <summary>
    /// 
    /// </summary>
    private int m_CollisionRegister_CollisionLayer = -1;
    /// <summary>
    /// True if currently manipulating a physics object
    /// </summary>
    private bool m_bPhysManipulation = false;

    /// <summary>
    /// Current jump state
    /// </summary>
    private Jump_State m_eJump = Jump_State.Grounded;
    private Grounded_State m_eGrounded = Grounded_State.AirBorne;
    #region Grounded Properties

    /// <summary>
    /// Anchor for grounded on a rigidbody
    /// </summary>
    private Transform m_Anchor;

    /// <summary>
    /// The Cosine of the angle of the anchor from the centre of the character controller at height of char controller radius
    /// </summary>
    private float m_fAnchorDot;
    /// <summary>
    /// The Cosine of the angle of the xz anchor offset and the desired direction of motion to determine relative motion to a slope
    /// </summary>
    private float m_fMotionDot;

    /// <summary>
    /// Anchor for grounded on a rigidbody
    /// </summary>
    private Vector3 m_v3AnchorOffset; // This may no longer be needed given actual skin thickness and collapsing to zero

    /// <summary>
    /// Spherecast for edge case grounding 
    /// </summary>
    private Ray m_RayForSpherecast = new Ray(Vector3.zero, -Vector3.up);


    private bool rayCastNormalWasDifferent = false;

    //float dotProductSurfaceDifferenceLimit = 0.71f; //(~45 Deg) isSurfing ignores this later
    float dotProductSurfaceDifferenceLimit = 0.495f; //(~60 Deg) isSurfing ignores this later

    public bool isSurfing = false;


    /// <summary>
    /// Local raycast result
    /// </summary>
    private RaycastHit t_SpherecastHit;

    /// <summary>
    /// Local raycast result
    /// </summary>
    private RaycastHit t_RaycastHit;

    #endregion

    /// <summary>
    /// Awake to set up the layers procedurally
    /// </summary>
    public void Awake()
    {
        // Find the first 'Empty' layer counting down from unity's limit of 32.
        if (m_CollisionRegister_CollisionLayer < 0)
        {
            // Find and store first empty layer for our colliders
            for (m_CollisionRegister_CollisionLayer = 31;
                LayerMask.LayerToName(m_CollisionRegister_CollisionLayer) != "";
                --m_CollisionRegister_CollisionLayer)
            {}
        }

        gameObject.layer = m_CollisionRegister_CollisionLayer;
        var colliders = GetComponentsInChildren<Collider>();
        for (int collider_index = 0;
            collider_index < colliders.Length;
            ++collider_index)
        {
            colliders[collider_index].gameObject.layer = m_CollisionRegister_CollisionLayer;
        }

        Physics.IgnoreLayerCollision(m_CollisionRegister_CollisionLayer, m_CollisionRegister_CollisionLayer, true);

        // Stops head jitters. :D but starts to clip :(
        //m_CharacterController.enableOverlapRecovery = false;
    }

    /// <summary>
    /// Called each frame to poll this Controllers inputs and move accordingly
    /// </summary>
    public void Update()
    {
        // Physics may have twisted us
        t_Vector3 = transform.eulerAngles;
        t_Vector3.x = 0;
        t_Vector3.z = 0;
        transform.eulerAngles = t_Vector3;


        float delta_time = Time.deltaTime;
        // Maintain Cursor lock
        Cursor.lockState = CursorLockMode.Locked;
        // Update character pose
        PoseUpdate(delta_time);
        // Mouse look - if not manipulating physics
        if (!m_bPhysManipulation)
        {
            Look(delta_time);
        }
        // Update character grounded state
        GroundedCheck();
        // Move the character
        Move(delta_time);
    }

    /// <summary>
    /// Update enumerable states
    /// </summary>
    /// <param name="a_fDeltaTime"></param>
    private void PoseUpdate(float a_fDeltaTime)
    {
        // Always goto couch on crouch key
        //if (m_BtnCrouch.CurrentState == ButtonState.Down)
        if (Input.GetButtonDown(m_BtnCrouch))
        {
            StopAllCoroutines();
            StartCoroutine(ControllerToCrouching());
        }

        // Prone Toggle
        if (Input.GetButtonDown(m_BtnProne))
        {
            StopAllCoroutines();
            if (m_ePose == Pose_State.Prone)
            {
                StartCoroutine(ControllerToStanding());
            }
            else
            {
                StartCoroutine(ControllerToProne());
            }
        }

        // If crouch was released and we didt go to prone from crouch,
        //   then stand.
        //if (m_BtnCrouch.CurrentState == ButtonState.Released &&
        //    m_ePose != Pose.Prone)
        if (!Input.GetButton(m_BtnCrouch) &&
             m_ePose != Pose_State.Prone &&
             m_ePose != Pose_State.Stand) // Added to negate running mario effect
        {
            if (!(m_ePreviousPose == Pose_State.Prone && m_bPoseLerping))
            {
                StopAllCoroutines();
                StartCoroutine(ControllerToStanding()); /// THis here starts EVERY FRAME in 'default'
            }
        }

        // If we were crouching and crouch is released, stand
        //if ((m_BtnCrouch.CurrentState == ButtonState.Released || m_BtnCrouch.CurrentState == ButtonState.None) &&
        if (!Input.GetButton(m_BtnCrouch) &&
            m_ePose == Pose_State.Crouch && !m_bPoseLerping)
        {
            StopAllCoroutines();
            StartCoroutine(ControllerToStanding());
        }
        
        // Update speed - Based on pose, keypresses, and whether airborne
        if (m_ePose == Pose_State.Stand)
        {
            //if (m_BtnSprint.CurrentState != ButtonState.None)
            if (Input.GetButton(m_BtnSprint))
            {
                m_fCurrentSpeed = m_fSprintSpeed;
            }
            //else if (m_BtnWalk.CurrentState != ButtonState.None)
            else if (Input.GetButton(m_BtnWalk))
            {
                m_fCurrentSpeed = m_fWalkSpeed;
            }
            else
            {
                m_fCurrentSpeed = m_fRunSpeed;
            }
        }
        else if (m_ePose == Pose_State.Crouch && m_eJump == Jump_State.Grounded)
        {
            m_fCurrentSpeed = m_fCrouchSpeed;
        }
        else if (m_ePose == Pose_State.Prone)
        {
            m_fCurrentSpeed = m_fProneSpeed;
        }
    }

    /// <summary>
    /// Rotates camera and controller as as per axes
    /// </summary>
    private void Look(float a_fDeltaTime)
    {
        // Rotation around Y ( Up )
        // float yRot = m_AxisLookX.AxisValue * a_fDeltaTime * m_LookSensitivity;
        float yRot = Input.GetAxis(m_AxisLookX) * a_fDeltaTime * m_fLookSensitivity;
        // Turn Left / Right
        transform.Rotate(Vector3.up, yRot, Space.Self);

        // Rotation around Local X ( Left/Right )
        // float xRot = m_AxisLookY.AxisValue * a_fDeltaTime * m_LookSensitivity;
        float xRot = Input.GetAxis(m_AxisLookY) * a_fDeltaTime * m_fLookSensitivity;
        // Look Up / Down
        m_POVCamera.transform.Rotate(transform.right, -xRot, Space.World);

        // If camera is upside down
        if (Vector3.Dot(m_POVCamera.transform.up, Vector3.up) < 0.0f)
        {
            // Looking up - Rolling Backwards
            if (xRot > 0)
            {
                m_POVCamera.transform.localEulerAngles = Vector3.right * -90.0f;
            }
            // Looking down - Tumbling Forwards
            else if (xRot < 0)
            {
                m_POVCamera.transform.localEulerAngles = Vector3.right * 90.0f;
            }
        }
    }

    /// <summary>
    /// Check the motion and triggers to determine if grounded
    /// </summary>
    /// <returns>True if grounded</returns>
    private void GroundedCheck()
    {
        rayCastNormalWasDifferent = false;

        if (m_HeightGrounded.Colliding && m_eJump != Jump_State.Jumping)
        {
            // Sphere cast to check find ground to anchor to
            // Cast from half height so we dont stand on an object on our head
            
            //m_SphereRaycast.origin = transform.position + (m_CharacterController.height * 0.5f - m_CharacterController.radius) * Vector3.up;
            m_RayForSpherecast.origin = transform.position + m_CharacterController.radius * Vector3.up;

            // Cast a sphere from within the controller to the base + 2x Skin width
            
            if (Physics.SphereCast(m_RayForSpherecast, m_CharacterController.radius, out t_SpherecastHit,
                //m_CharacterController.height * 0.5f - (m_CharacterController.radius * 2) + // The size of the sphere is added to each end of the 'ray'
                //0.15f, // THIS IS 3 x Difference of Char controlle rradius and the gounded controller
                m_fSkinThickness,//m_CharacterController.skinWidth * 2, // Doubling down on skin width in case of perfect collision / Unity.Grounded
                ~(1 << gameObject.layer)))
                //~(1 << LayerMask.NameToLayer("Player"))))
            {
                ///* Raycast past the point of contact looking for a collision normal. If it was a flat up surface we found, do not 
                /// 
                Vector3 origin = t_SpherecastHit.point - transform.position;
                //Debug.Log(transform.position + " " + m_SpherecastResult.point);


                origin.y = 0.0f;
                //Debug.Log(origin + " " + origin.magnitude);

                // Anchor is not directly below
                if (origin.magnitude > m_fSkinThickness)
                {
                    origin = origin.normalized * m_CharacterController.radius;
                    RaycastHit rch; 
                    Ray raycast = new Ray(transform.position + origin + Vector3.up * m_CharacterController.radius, -Vector3.up);
                    Debug.DrawRay(transform.position + origin + Vector3.up * m_CharacterController.radius, -Vector3.up, Color.red);
                    if (Physics.Raycast(raycast, out rch, m_CharacterController.radius + m_fSkinThickness, ~(1 << gameObject.layer)))
                    {
                        //Debug.Log(m_SpherecastResult.normal + " " + rch.normal);
                        // If Normal of surface is different to spherecast, idk... 
                        if (Vector3.Dot(t_SpherecastHit.normal, rch.normal) < dotProductSurfaceDifferenceLimit)
                        {
                            Debug.Log("Anchor is edge, not slope");
                            rayCastNormalWasDifferent = true;
                            // stepping = true;
                            // Dont rotate motion!? Go flat "To it"
                        }
                    }
                }
                // World space position of a raycast down along the outer skin of the character controller.

                

                //if (Physics.Raycast(m_SphereRaycast, ))
                //if (Vector3.Dot(m_RaycastResult.normal, Vector3.up) < 0.9995f)
                //{
                //    // Not up, extrapolate and check surface type
                //
                //}
                //else
                //{
                    // END FANCY EXTRA RAYCAST

                    // If there was overlap, setup the anchor
                    m_Anchor.gameObject.SetActive(true);
                    m_Anchor.transform.position = t_SpherecastHit.point;
                    m_Anchor.parent = t_SpherecastHit.collider.transform;
                    transform.parent = t_SpherecastHit.collider.transform; // Crazy idea to support moving platforms... and it works


               // Debug.Log(m_Anchor.parent.name.ToString());

                    // Record the current offset to the anchor
                    m_v3AnchorOffset = transform.position - m_Anchor.transform.position;

                // Push the controller out of the object based on the displacement to the anchor 
                t_Vector3 = m_v3AnchorOffset + Vector3.up * (m_CharacterController.radius - m_fSkinThickness);// m_CharacterController.skinWidth);

                    float ratio = t_Vector3.magnitude / (m_CharacterController.radius);

                    //This here is the moment the player is clamped to the surface.
                    transform.position += (1 - ratio) * t_Vector3; //// DOESNT ALL THIS COLLAPS BACK TO += 0!
                    //Debug.Log(ratio + " " + t_Vector3.ToString());

                    // Record the final offset
                    m_v3AnchorOffset = transform.position - m_Anchor.transform.position;

                    m_eJump = Jump_State.Grounded;
                //}
                //m_fYVelocity = 0.0f;
            }
            else
            {
                // Airborne?
                m_eJump = Jump_State.InAir;


            }

            // If the anchor is active, Adjust the controller and set falling to 0 if flat enough
            //if (m_Anchor.gameObject.activeInHierarchy)
            //{
            //    if (m_eJump == Jump.InAir)
            //        Debug.Log("Air Yet Active?");
            //    // Set pos by anchor
            //    //transform.position = m_Anchor.transform.position + m_v3AnchorOffset;
            //    //Debug.Log("here");
            //    m_eJump = Jump.Grounded;
            //    m_v3LaunchVelocity = Vector3.zero; // Assigned to but never used.
            //
            //}
        }
        // In the air but not jumping state
        // Walked off an edge or reached the peak of a jump.
        else if (m_eJump != Jump_State.Jumping)
        {
            m_eJump = Jump_State.InAir;
            if (m_Anchor.gameObject.activeInHierarchy) // Disable on walk off edge
            {
                // Disabling these allows you to stick to anything... 
                // Perhaps if we check dotProd. We can just stick to down
                // Disable if Anchor within...

                m_Anchor.parent = transform;
                m_Anchor.localPosition = Vector3.zero;
                m_Anchor.gameObject.SetActive(false);
            }
            transform.parent = null;
        }
    }   

    /// <summary>
    /// Moves the character controller in local space
    /// </summary>
    /// <param name="a_fDeltaTime"></param>
    private void Move(float a_fDeltaTime)
    {
        // Get axes w/ weights
        //m_v3Motion = new Vector3(m_AxisMoveX.AxisValue, 0, m_AxisMoveZ.AxisValue);
        //m_v3Motion = new Vector3(Input.GetAxis(m_AxisMoveX), 0, Input.GetAxis(m_AxisMoveZ));
        m_v3Motion = new Vector3(Input.GetAxis(m_AxisMoveX), 0, Input.GetAxis(m_AxisMoveZ));
        
        // Ignore them if we are manipulating an object
        if (m_bPhysManipulation)
        {
            m_v3Motion = Vector3.zero;
        }
        
        // Normalise the vector if it is > 1
        if (m_v3Motion.sqrMagnitude > 1)
        {
            m_v3Motion.Normalize();
        }

        // Scale by speed - Component wise, post input scaling
        // Forwards
        m_v3Motion.z *= m_fCurrentSpeed;
        // Backwards
        if (m_v3Motion.z < 0)
        {
            m_v3Motion.z *= m_fReverseRatio;
        }
        // Strafe
        m_v3Motion.x *= m_fCurrentSpeed * m_fStrafeRatio;

        // Transform the vector into relative motion
        m_v3Motion = transform.TransformDirection(m_v3Motion);
        // Reduce input by ratio if airborne
        if (m_eJump != Jump_State.Grounded)
        {
            //m_v3Motion *= m_fAirborneRatio; // And clamp to initial XZ Velocity at time of jump instead.
        }

        // If jump pressed and jump hitbox is clear, jump.
        //if ((m_BtnJump.CurrentState == ButtonState.Down) && (m_eJump == Jump.Grounded))
        if (Input.GetButtonDown(m_BtnJump) && (m_eJump == Jump_State.Grounded))
        {
            // Head butt things
            if (m_HeightJump.Colliding)
            {
                m_HeightJump.ForceOutRigidbodies(ForceMode.Impulse);
            }
            else // Jump with conditions
            {
                // Set jump speed if on a slope less that slope limit
                t_Vector3 = m_Anchor.transform.position - (transform.position + Vector3.up * (m_CharacterController.radius - m_CharacterController.skinWidth));

                // If collision tangent is < slope, we can jump
                m_fAnchorDot = Vector3.Dot(t_Vector3.normalized, -Vector3.up);

                m_fAnchorDot = Mathf.Clamp(m_fAnchorDot, 0.0f, 1.0f);

                //if (m_fAnchorDot > Mathf.Cos(m_CharacterController.slopeLimit * RADIANS_TO_DEGREES))
                //{
                // Add jump speed to Y
                //m_fYVelocity = m_fJumpSpeed;
                if (m_fYVelocity < 0)
                        m_fYVelocity = m_fJumpSpeed;
                    else if(m_fAnchorDot >= dotProductSurfaceDifferenceLimit) // We can jump up things we can walk up
                        m_fYVelocity += m_fJumpSpeed * m_fAnchorDot; // Testing accumulation * as rate of incline


                    // Update state
                    m_eJump = Jump_State.Jumping;
                    // Scale jump if crouching...
                    if (m_ePose == Pose_State.Crouch)
                    {
                        m_fYVelocity *= m_fCrouchJumpRatio;
                    }
                    // .. or prone.
                    if (m_ePose == Pose_State.Prone)
                    {
                        m_fYVelocity *= m_fProneJumpRatio;
                    }
                    // Store the X/Z of projectile motion
                    m_v3LaunchVelocity = m_CharacterController.velocity;
                    m_v3LaunchVelocity.y = 0.0f;
                //}

                if (m_Anchor.gameObject.activeInHierarchy) // Disable on valid jump
                {
                    // Disabling these allows you to stick to anything... 
                    // Perhaps if we check dotProd. We can just stick to down
                    // Disable if Anchor within...

                    m_Anchor.parent = transform;
                    m_Anchor.localPosition = Vector3.zero;
                    m_Anchor.gameObject.SetActive(false);
                }
                transform.parent = null;
            }
        }
        // else apply gravity
        else if (m_eJump != Jump_State.Grounded 
            && !stepping) // QUESTIONABLE
        {
            Debug.Log("Not Grounded");
            // Apply gravity
            m_fYVelocity = m_fYVelocity + m_fGravity * a_fDeltaTime;

            // Roof hit check
            //if (m_fYVelocity > 0 && m_HeightJump.Colliding) // AND NOT GROUNDED LOL oops
            if (m_fYVelocity > 0 && m_HeightJump.Colliding) // AND NOT GROUNDED LOL oops
            {
                m_fYVelocity = 0;
                Debug.Log(m_HeightJump.Colliding);
                m_eJump = Jump_State.InAir;
                m_HeightJump.ForceOutRigidbodies(ForceMode.Impulse);
            }
            // Check y speed and jump state to transition to in-air
            if (m_eJump == Jump_State.Jumping)
            {
                // Toggle from jump to falling
                if (m_fYVelocity <= 0)
                {
                    m_eJump = Jump_State.InAir;
                }
            }
        }
        else // Grounded
        {
            Debug.Log("Grounded");
            t_Vector3 = m_Anchor.transform.position - (transform.position + Vector3.up * (m_CharacterController.radius - m_CharacterController.skinWidth));
            // If collision tangent is < slope, we can 'stick' by wiping off velocity
            m_fAnchorDot = Vector3.Dot(t_Vector3.normalized, -Vector3.up);

            m_fAnchorDot = Mathf.Clamp(m_fAnchorDot, 0.0f, 1.0f);

            //if (!(m_fAnchorDot > Mathf.Cos(m_CharacterController.slopeLimit * RADIANS_TO_DEGREES)))
            //{
            //    // Becomes velocity down the slope
            //    m_fYVelocity = m_fYVelocity + m_fGravity * a_fDeltaTime * (1 - m_fAnchorDot);
            //
            //    // Z/X Motion 
            //    //t_Vector3 = m_Anchor.transform.position - transform.position;
            //    t_Vector3.y = 0;
            //    t_Vector3.Normalize();
            //    t_Vector3 *= m_fYVelocity * (m_fAnchorDot);
            //
            //    // Wipe out motion into the surface...
            //    m_v3Motion -=
            //        (m_Anchor.transform.position - transform.position).normalized *
            //        Vector3.Dot((m_Anchor.transform.position - transform.position).normalized, m_v3Motion.normalized) *
            //        m_v3Motion.magnitude;
            //
            //    // Apply Reacrtion force
            //    m_v3Motion += t_Vector3;
            //    //Debug.Log(t_Vector3.ToString());
            //}
            //else // we walk around on a shallow. no slope
            {
                // Direction to the quad highside
                t_Vector3 = m_Anchor.transform.position - transform.position;
                t_Vector3.y = 0;
                t_Vector3.Normalize();

                m_fMotionDot = Vector3.Dot(m_v3Motion.normalized, t_Vector3);
                // Rotate by 90 around Y
                float t = t_Vector3.x;
                t_Vector3.x = -t_Vector3.z;
                t_Vector3.z = t;

                /*
                // Rotate motion around t_Vector3 by acos(m_fAnchorDot), slightly into the surface
                //if (m_v3Motion.magnitude < m_fSprintSpeed) // Essentially - if not sprinting, stick
                if (fMotionDot < 0) // Moving down the plane
                    {
                        m_v3Motion = Quaternion.AngleAxis(Mathf.Acos(m_fAnchorDot) / RADIANS_TO_DEGREES + ROTATION_PADDING, t_Vector3) * m_v3Motion;
                        //if (m_fYVelocity < 0) // This stops us snapping over edges - Apparently not
                        //{
                        //    //m_fYVelocity = m_v3Motion.y;
                        //}
                        //else
                        //{
                        //    // Nup - A frame or two continuing forward 'cresting' nope... probs nead to measure deltaDots for anchor dot.
                        //}
                    }
                    else // Moving along or up the plane
                    {
                        //m_v3Motion = Quaternion.AngleAxis(Mathf.Acos(m_fAnchorDot) / RADIANS_TO_DEGREES - ROTATION_PADDING, t_Vector3) * m_v3Motion;
                        m_v3Motion = Quaternion.AngleAxis(Mathf.Acos(m_fAnchorDot) / RADIANS_TO_DEGREES - ROTATION_PADDING, t_Vector3) * m_v3Motion;
                        //m_fYVelocity = m_v3Motion.y;
                    }
                */

                //#region Full stick! Full Speed!

                // Rotate motion around t_Vector3 by acos(m_fAnchorDot), slightly into the surface
                //if (m_v3Motion.magnitude < m_fSprintSpeed) // Essentially - if not sprinting, stick
                if (!rayCastNormalWasDifferent || isSurfing)
                {
                    if (m_fMotionDot <= 0) // Moving down the plane
                    {
                        m_v3Motion = Quaternion.AngleAxis(Mathf.Acos(m_fAnchorDot) / RADIANS_TO_DEGREES + ROTATION_PADDING, t_Vector3) * m_v3Motion;
                        //if (m_fYVelocity < 0) // This stops us snapping over edges - Apparently not
                        //{
                        //m_fYVelocity = m_v3Motion.y;
                        //}
                        //else
                        //{
                        //    // Nup - A frame or two continuing forward 'cresting' nope... probs nead to measure deltaDots for anchor dot.
                        //}
                    }
                    else // Moving along or up the plane
                    {
                        //m_v3Motion = Quaternion.AngleAxis(Mathf.Acos(m_fAnchorDot) / RADIANS_TO_DEGREES - ROTATION_PADDING, t_Vector3) * m_v3Motion;
                        m_v3Motion = Quaternion.AngleAxis(Mathf.Acos(m_fAnchorDot) / RADIANS_TO_DEGREES - ROTATION_PADDING, t_Vector3) * m_v3Motion;
                        //m_fYVelocity = m_v3Motion.y;
                        // Slow motion as a direct proportion of slope
                        //Debug.Log(Vector3.Dot(m_v3Motion, Vector3.up));
                        if (!isSurfing)
                        {
                            Debug.Log("Speed as a f() of incline");
                                m_v3Motion = m_v3Motion * (1 - Vector3.Dot(m_v3Motion.normalized, Vector3.up));
                            if (m_fAnchorDot >= dotProductSurfaceDifferenceLimit) // Only if shallow enough
                            {

                            }
                            else
                            {
                                m_v3Motion.y = 0.0f;
                            }
                        }
                    }
                }
                else // rayCastNormalWasDifferent && not surfing
                {
                    // ANCHOR IS ON A STEP
                    //Debug.Log("Step below detected");
                }
                

                //#endregion

                m_fYVelocity = m_v3Motion.y;
            }
        }

        // Assign from velocity
        m_v3Motion.y = m_fYVelocity;

        // If theres any motion, disable the anchor till next frame.
        // This probably needs work for moving platforms
        //if (m_v3Motion.magnitude > float.Epsilon)
        //{
        //    if (m_Anchor.gameObject.activeInHierarchy)
        //    {
        //        // Disabling these allows you to stick to anything... 
        //        // Perhaps if we check dotProd. We can just stick to down
        //        // Disable if Anchor within...
        //        //if ((m_Anchor.position - transform.position).magnitude > 0.1f)
        //        //{
        //            m_Anchor.parent = transform;
        //            m_Anchor.localPosition = Vector3.zero;
        //            m_Anchor.gameObject.SetActive(false);
        //        //}
        //    }
        //}

        // Only d
        //if (m_v3Motion.magnitude > float.Epsilon)
        //{
        //    if (m_Anchor.gameObject.activeInHierarchy)
        //    {
        //        // Disabling these allows you to stick to anything... 
        //        // Perhaps if we check dotProd. We can just stick to down
        //        // Disable if Anchor within...
        //        
        //        m_Anchor.parent = transform;
        //        m_Anchor.localPosition = Vector3.zero;
        //        m_Anchor.gameObject.SetActive(false);
        //    }
        //}

        /*
        #region Step Detection
        //Finally, before applying motion, see if we also have to add some step up motion if detected
        // Sphere cast on xz motion to skin thickness, 
        if (!rayCastNormalWasDifferent)
        {
            stepping = false;
        }
        t_Vector3 = m_v3Motion;
        t_Vector3.y = 0.0f;
        if (t_Vector3.sqrMagnitude > 0.0f)
        {
            t_Vector3.Normalize();

            //t_Vector3 = t_Vector3 * plusSkin; // This is the projection vector.

            Vector3 t_origin = transform.position + m_CharacterController.radius * Vector3.up;

            Ray raaaay = new Ray(t_origin, t_Vector3);

            float fBase = transform.position.y;
            float fStep = fBase + m_fStepHeight + plusSkin;

             // Any edges ahead of us in the direction of motion.
             RaycastHit t_hitInfo;
            if (Physics.SphereCast(raaaay, m_CharacterController.radius, out t_hitInfo, plusSkin, ~(1 << gameObject.layer)))
            {
                Debug.Log("FWD CAST HIT");
                // Now cast down onto the hit point from the max step height + 'skin'
                t_origin = t_hitInfo.point;
                t_origin.y = fStep;
                // Cast onto hit point looking for step.
                RaycastHit t_secondaryRCH;
                if (Physics.Raycast(t_origin, Vector3.down, out t_secondaryRCH, m_fStepHeight, ~(1 << gameObject.layer)))
                {
                    Debug.Log("DOWN CAST HIT");
                    // Check its not a/the slope we are standing on.
                    Vector3 anchorToPoint = (m_Anchor.position - t_secondaryRCH.point);
                    //if (anchorToPoint.sqrMagnitude > plusSkin)
                    //{
                        Debug.Log("Anchor is not Cast Hit");
                        anchorToPoint.Normalize();
                        if (Mathf.Abs(Vector3.Dot(t_hitInfo.normal, anchorToPoint)) < plusSkin)
                        {
                            Debug.Log("PLANE DETECTED CAST HIT");
                            // Point is on the plane the anchor is. Do nothing
                            // or on an edge... :/
                            // Ray cast above and fw, check normal agains raycast forward but below normal.
                                                      

                        }
                        else // Check theres enough room above it AND the current player to step
                        {
                            Debug.Log("Different Normals");
                            if (Physics.Raycast(t_origin, Vector3.up, out t_hitInfo, m_CharacterController.height, ~(1 << gameObject.layer)))
                            {
                                // Nothing, no space
                            }
                            else
                            {
                                stepping = true;
                            }
                        }
                    //}
                }


                // Then cast 
            }

        }
        #endregion

        #region Step Up
        if (!m_HeightJump.Colliding && stepping && m_v3Motion.magnitude > plusSkin)
        {
            m_v3Motion.y += m_fStepSpeed;
            //m_v3Motion.y += m_fJumpSpeed;
            Debug.Log("STEPPING!");
        }
        #endregion
        */
        // Apply Motion
        CollisionFlags cf;
        if (m_CharacterController.enabled)
        {
            m_v3Motion *= a_fDeltaTime;
            cf = m_CharacterController.Move(m_v3Motion);
        }
        //else // Fly mode
        //{
        //    transform.Translate(m_v3Motion * a_fDeltaTime);
        //}

        //cf.Sid
        // Stepping now.
        // If the character controller's motion, 
    }

    /// <summary>
    /// How to react on controller collision with rigid body object
    /// </summary>
    /// <param name="a_collision"></param>
    private void OnControllerColliderHit(ControllerColliderHit a_collision)
    {
        // Get the contacted rigidbody
        t_RigidBody = null;
        t_RigidBody = a_collision.transform.GetComponent<Rigidbody>();

        // Apply force based on layer and reletive displacement
        if (t_RigidBody != null)
        {
            // Displacement calculation to collision point
            t_Vector3 = a_collision.point - transform.position;
            //t_Vector3 = (t_RigidBody.position + t_RigidBody.centerOfMass) - transform.position;
            t_Vector3.y = 0;
            t_Vector3.Normalize();              // RELATIVE GOD DAMN VELOCITY

            t_fDot = Vector3.Dot(t_Vector3, m_v3Motion);// a_collision.moveDirection.normalized);

            // Apply force to object if valid, as a function of motion, and strength
            if (!t_RigidBody.isKinematic)
            {
                if (t_fDot > 0 && t_RigidBody.mass <= m_fPushWeight) // Changem_fPushWeight  to mass * Motion speed
                {
                    t_RigidBody.AddForceAtPosition( // Change m_fPushWeight to mass * Motion speed
                            t_Vector3 * t_fDot * (m_fPushWeight / Mathf.Max( 10.0f, t_RigidBody.mass)),
                            //t_Vector3 * t_fDot * Mathf.Clamp((PushStrength / t_RigidBody.mass), 0.0f , m_fCurrentSpeed * 0.5f),
                            //a_collision.normal * -t_fDot,
                            //a_collision.moveDirection,
                            //t_Vector3 * t_fDot,// * PushStrength,// * Motion.magnitude,
                            //a_collision.normal * t_fDot * PushStrength,
                            (t_RigidBody.position + t_RigidBody.centerOfMass),//a_collision.point,
                            //a_collision.point,//(t_RigidBody.position + t_RigidBody.centerOfMass),
                            ForceMode.VelocityChange);
                }
            }
        }
    }


    #region Character Controller Height Lerping
    /// <summary>
    /// Co-routine to lerp the controller to standing size
    /// </summary>
    private IEnumerator ControllerToStanding()
    {
        // Get the target height from the heightStand trigger
        float fFinishHeight = m_HeightStand.GetComponent<CapsuleCollider>().height;
        // Wait untill clear to stand
        bool bCantStand = true;
        while (bCantStand)
        {
            // If grounded, check the heightStand trigger
            if (m_eJump == Jump_State.Grounded)
            {
                bCantStand = m_HeightStand.Colliding;
                if (m_HeightStand.OnlyDynamicCollisions)
                {
                    m_HeightStand.ForceOutRigidbodies();
                }
            }
            // Not grounded, then there is space
            else
            {
                bCantStand = false;
            }
            // Keep waiting.
            yield return new WaitForEndOfFrame();
        }

        // Store the previous pose
        m_ePreviousPose = m_ePose;
        // Set the current pose
        m_ePose = Pose_State.Stand;
        // Initialise timer
        m_fPoseTimer = 0.0f;
        // Set the lerping flag
        m_bPoseLerping = true;
        // Current height is stored as the start height as to lerp from any beginning
        float fStartHeight = m_CharacterController.height;
        // Continue to lerp towards target height while unobstructed
        while (m_fPoseTimer < m_fPoseTime)
        {
            // If new collisions detected, wait.
            if (m_HeightStand.Colliding)
            {
                yield return new WaitForEndOfFrame();
                if (m_HeightStand.OnlyDynamicCollisions)
                {
                    m_HeightStand.ForceOutRigidbodies();
                }
            }
            else
            {
                // ... else Lerp
                m_fPoseTimer += Time.deltaTime;
                m_CharacterController.height = Mathf.Lerp(fStartHeight, fFinishHeight, m_fPoseTimer / m_fPoseTime);
                m_CharacterController.center = Vector3.up * m_CharacterController.height * 0.5f;
                m_POVCamera.transform.parent.localPosition = Vector3.up * (1 - m_fEyeDepthRatio) * m_CharacterController.height;
                yield return new WaitForEndOfFrame();
            }
        }

        // Once finished, snap to height, centre, and camera, to remove any inaccuracy
        m_CharacterController.height = fFinishHeight;
        m_CharacterController.center = Vector3.up * m_CharacterController.height * 0.5f;
        m_POVCamera.transform.parent.localPosition = Vector3.up * (1 - m_fEyeDepthRatio) * m_CharacterController.height;
        m_bPoseLerping = false;
    }

    /// <summary>
    /// Co-routine to lerp the controller to crouch size
    /// </summary>
    private IEnumerator ControllerToCrouching()
    {
        // Get the target height from the heightCrouch trigger
        float fFinishHeight = m_HeightCrouch.GetComponent<CapsuleCollider>().height;

        // Wait until it is clear.
        //   This should only be colliding if character is prone and under an object
        while (m_HeightCrouch.Colliding)
        {
            if (m_HeightCrouch.OnlyDynamicCollisions)
            {
                m_HeightCrouch.ForceOutRigidbodies();
            }
            yield return new WaitForEndOfFrame();
        }

        // Store the previous pose
        m_ePreviousPose = m_ePose;
        // Set the current pose
        m_ePose = Pose_State.Crouch;
        // Initialise timer
        m_fPoseTimer = 0.0f;
        // Set the lerping flag
        m_bPoseLerping = true;
        // Current height is stored as the start height as to lerp from any beginning
        float fStartHeight = m_CharacterController.height;

        // Continue to lerp towards target height while unobstructed
        while (m_fPoseTimer < m_fPoseTime)
        {
            // If new collisions detected, wait.
            if (m_HeightCrouch.Colliding)
            {
                if (m_HeightCrouch.OnlyDynamicCollisions)
                {
                    m_HeightCrouch.ForceOutRigidbodies();
                }
                yield return new WaitForEndOfFrame();
            }
            else
            {
                // We track the previous and new lerps in order to calculate the lifting of legs in a crouch jump
                float previousLerp = Mathf.Lerp(fStartHeight, fFinishHeight, m_fPoseTimer / m_fPoseTime);
                // Update pose timer
                m_fPoseTimer += Time.deltaTime;
                float newLerp = Mathf.Lerp(fStartHeight, fFinishHeight, m_fPoseTimer / m_fPoseTime);

                // Update height and centre
                m_CharacterController.height = newLerp;
                m_CharacterController.center = Vector3.up * m_CharacterController.height * 0.5f;
                m_POVCamera.transform.parent.localPosition = Vector3.up * (1 - m_fEyeDepthRatio) * m_CharacterController.height;

                // If crouching in the air, Lift the legs by moving the whole collider up.
                if (m_eJump != Jump_State.Grounded)
                {
                    m_CharacterController.Move(Vector3.up * (previousLerp - newLerp));
                }    
                
                yield return new WaitForEndOfFrame();
            }
        }

        // Once finished, snap to height, centre, and camera, to remove any inaccuracy
        m_CharacterController.height = fFinishHeight;
        m_CharacterController.center = Vector3.up * m_CharacterController.height * 0.5f;
        m_POVCamera.transform.parent.localPosition = Vector3.up * (1 - m_fEyeDepthRatio) * m_CharacterController.height;
        m_fPoseTimer = 0.0f;
        m_bPoseLerping = false;
    }

    /// <summary>
    /// Coroutine for changing the shape of the collider to the prone size.
    /// </summary>
    private IEnumerator ControllerToProne()
    {
        // Start prone lerp immediately, prone collider should never be occupied
        m_fPoseTimer = 0.0f;
        m_bPoseLerping = true;
        m_ePose = Pose_State.Prone;
        // Current height is stored as the start height as to lerp from any beginning
        float fStartHeight = m_CharacterController.height;
        // Get the target height from the heightStand trigger
        float fFinishHeight = m_HeightProne.GetComponent<CapsuleCollider>().height;

        // Lerp as a function of time and framerate
        while (m_fPoseTimer < m_fPoseTime)
        {
            m_fPoseTimer += Time.deltaTime;
            m_CharacterController.height = Mathf.Lerp(fStartHeight, fFinishHeight, m_fPoseTimer / m_fPoseTime);
            m_CharacterController.center = Vector3.up * m_CharacterController.height * 0.5f;
            m_POVCamera.transform.parent.localPosition = Vector3.up * (1 - m_fEyeDepthRatio) * m_CharacterController.height;
            yield return new WaitForEndOfFrame();
        }

        // Once finished, snap to height, centre, and camera, to remove any inaccuracy
        m_CharacterController.height = fFinishHeight;
        m_CharacterController.center = Vector3.up * m_CharacterController.height * 0.5f;
        m_POVCamera.transform.parent.localPosition = Vector3.up * (1 - m_fEyeDepthRatio) * m_CharacterController.height;
        m_fPoseTimer = 0.0f;
        m_bPoseLerping = false;
    }
    #endregion
}

